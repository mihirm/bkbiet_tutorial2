package com.optimizory.tutorial;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DatabaseAccess {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Connection connection = null;
		String selectSQL = "SELECT * FROM bkbiet";
		String insertTableSQL = "INSERT INTO bkbiet"
				+ "(name, stream, email) VALUES"
				+ "(?,?,?)";
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			connection = DriverManager
			.getConnection("jdbc:mysql://localhost:3306/bkbiet","mihir", "mihir123");
			

	 
		} catch (SQLException e) {
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;
		}
		
		if(connection != null) {
			System.out.println("You made it, take control your database now!");
			
			PreparedStatement preparedStatement = null;
			
			try {
				
				preparedStatement = connection.prepareStatement(insertTableSQL);
				
				preparedStatement.setString(1, "Mihir");
				preparedStatement.setString(2, "opt");
				preparedStatement.setString(3, "me@myself.com");
				
				preparedStatement.execute();
				
				preparedStatement = connection.prepareStatement(selectSQL);
				
				ResultSet rs = preparedStatement.executeQuery();
				
				while(rs.next()) {
					String db_name = rs.getString("name");
					String db_stream = rs.getString("stream");
					String db_email = rs.getString("email");
					System.out.println(db_name+ " "+ db_stream +" "+ db_email);
					
				}
				

				
			}catch(SQLException sqle) {
				sqle.printStackTrace();
			}
			
		} else {
			System.out.println("Failed to make connection!");
		}
		


	}

}
